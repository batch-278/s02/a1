from django.shortcuts import render
from django.http import HttpResponse
from .models import GroceryItem
from django.template import loader

# Create your views here.

def groceries(req):
    grocery_items = GroceryItem.objects.all()
    template = loader.get_template("index.html")
    context = {
        'grocery_list': grocery_items
    }
    return HttpResponse(template.render(context, req))

def groceryitem(req, groceryitem_id):
    response  = f"You are viewing the details of grocery item {groceryitem_id}"
    return HttpResponse(response)