from django.urls import path
from . import views

urlpatterns = [
    path("list", views.groceries, name="grocery list"),
   path("<int:groceryitem_id>", views.groceryitem, name="viewgroceryitem")
]
